package br.edu.controller;

import java.util.HashSet;
import java.util.Set;

import br.edu.model.Cliente;

public class Controll {

	public static Controll controll=null;
	
	private Controll(){}

	private Set<Cliente> clientes = new HashSet<Cliente>();
	
	public static Controll getInstance() throws Exception{
		if (controll == null) {
			controll = new Controll();
		} else {
			throw new Exception("Controlador j� existe!!");
		}
		return controll;
	}
	
	/**
	 * Exemplo de desenvolvimento de caso de uso!!
	 * @param nome
	 * @param endereco
	 * @param cpf
	 */
	public void adicionaCliente(String nome, String endereco, String cpf,String telefone){
		Cliente temp = new Cliente();
		temp.setNome(nome);
		temp.setEndereco(endereco);
		temp.setCpf(cpf);
		temp.setTelefone(telefone);
		//if (! clientes.contains(temp)){ //Pr�-condi��o
		clientes.add(temp);
		//}
	}
	
	public Set<Cliente> listarClientes(){
		return clientes;
	}
	
}
