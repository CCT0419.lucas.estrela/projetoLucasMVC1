package br.edu.view;

import java.util.Scanner;

import br.edu.controller.Controll;
import br.edu.model.Cliente;

public class Principal {

	public static void main(String[] args) throws Exception {
		Scanner scr = new Scanner(System.in);
		Controll controlador = Controll.getInstance();
		for (int i=0;i<2;i++){
			System.out.println("Digite um nome: ");
			String nome = scr.next();
			System.out.println("Digite um cpf");
			String cpf = scr.next();
			System.out.println("Digite um telefone");
			String telefone = scr.next();
			System.out.println("Digite um endereco");
			String endereco = scr.next();
			//Controll ctrl2 = Controll.getInstance();	
			controlador.adicionaCliente(nome, endereco, cpf, telefone);
			System.out.println("Cliente adicionado!!");
			System.out.println();
		}
		
		System.out.println("Clientes cadastrados:");
		for (Cliente cliente: controlador.listarClientes()){
			System.out.println("Nome: "+cliente.getNome());
			System.out.println("Cpf: "+cliente.getCpf());
			System.out.println("Endereco: "+cliente.getEndereco());
			System.out.println("Telefone: "+cliente.getTelefone()+"\n");
		}
		
		scr.close();
	}

}
