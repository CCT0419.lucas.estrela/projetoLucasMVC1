package br.edu.model;

public class Produto extends Cliente{
	private String nomeProd;
	private String descricao;
	private double valorProd;
	
	public String getNomeProd() {
		return nomeProd;
	}
	public void setNomeProduto(String pNomeProd) {
		this.nomeProd=pNomeProd;
	}
	public String getDescricaoprod() {
		return descricao;
	}
	public void setNomeProd(String pDescricaoProd) {
		this.descricao=pDescricaoProd;
	}
	public double getValor() {
		return valorProd;
	}
	public void setValor(double pValorProd) {
		this.valorProd=pValorProd;
	}
}
